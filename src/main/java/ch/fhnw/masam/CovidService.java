package ch.fhnw.masam;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CovidService {

	private static final Logger LOG = LogManager.getLogger(CovidService.class);
	
	@Autowired
    private CovidRepository covidRepository;
    
    public List<CovidCase> findByDate(String date) {
    	return covidRepository.findByDate(date);
 
    }
    
    public List<CovidCase> findByCountry(String country) {
    	LOG.debug("Debug Rigi Kaltbad Einfach");
    	return covidRepository.findByCountry(country);
    }
    
    public List<String> getAllCountries() {
    	return covidRepository.getAllCountries();
    }
	
}
