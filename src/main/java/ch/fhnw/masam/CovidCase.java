package ch.fhnw.masam;

// Test Manuel 2
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tcovid_cases")
public class CovidCase {

	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "Case_Type")
	private String caseType;
	
	@Column(name = "People_Total_Tested_Count")
	private Integer peopleTotalTestedCount;
	
	@Column(name = "Cases")
	private Integer cases;
	
	@Column(name = "Difference")
	private Integer difference;
	
	@Column(name = "Date")
	private Date date;
	
	@Column(name = "Combined_Key")
	private String combinedKey;
	
	@Column(name = "Country_Region")
	private String countryRegion;
	
	@Column(name = "Province_State")
	private String provinceState;
	
	@Column(name = "Admin2")
	private String admin2;
	
	@Column(name = "iso2")
	private String iso2;
	
	@Column(name = "iso3")
	private String iso3;
	
	@Column(name = "FIPS")
	private String fips;
	
	@Column(name = "Latitude")
	private Float latitude;
	
	@Column(name = "Longitude")
	private Float longitude;
	
	@Column(name = "Population_Count")
	private Integer populationCount;
	
	@Column(name = "People_Hospitalized_Cumulative_Count")
	private Integer peopleHospitalized;
	
	@Column(name = "Data_Source")
	private String dataSource;
	
	@Column(name = "Prep_Flow_Runtime")
	private String prepFlowRuntime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public Integer getPeopleTotalTestedCount() {
		return peopleTotalTestedCount;
	}

	public void setPeopleTotalTestedCount(Integer peopleTotalTestedCount) {
		this.peopleTotalTestedCount = peopleTotalTestedCount;
	}

	public Integer getCases() {
		return cases;
	}

	public void setCases(Integer cases) {
		this.cases = cases;
	}

	public Integer getDifference() {
		return difference;
	}

	public void setDifference(Integer difference) {
		this.difference = difference;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCombinedKey() {
		return combinedKey;
	}

	public void setCombinedKey(String combinedKey) {
		this.combinedKey = combinedKey;
	}

	public String getCountryRegion() {
		return countryRegion;
	}

	public void setCountryRegion(String countryRegion) {
		this.countryRegion = countryRegion;
	}

	public String getProvinceState() {
		return provinceState;
	}

	public void setProvinceState(String provinceState) {
		this.provinceState = provinceState;
	}

	public String getAdmin2() {
		return admin2;
	}

	public void setAdmin2(String admin2) {
		this.admin2 = admin2;
	}

	public String getIso2() {
		return iso2;
	}

	public void setIso2(String iso2) {
		this.iso2 = iso2;
	}

	public String getIso3() {
		return iso3;
	}

	public void setIso3(String iso3) {
		this.iso3 = iso3;
	}

	public String getFips() {
		return fips;
	}

	public void setFips(String fips) {
		this.fips = fips;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public Integer getPopulationCount() {
		return populationCount;
	}

	public void setPopulationCount(Integer populationCount) {
		this.populationCount = populationCount;
	}

	public Integer getPeopleHospitalized() {
		return peopleHospitalized;
	}

	public void setPeopleHospitalized(Integer peopleHospitalized) {
		this.peopleHospitalized = peopleHospitalized;
	}

	public String getDataSource() {
		return dataSource;
	}

	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	public String getPrepFlowRuntime() {
		return prepFlowRuntime;
	}

	public void setPrepFlowRuntime(String prepFlowRuntime) {
		this.prepFlowRuntime = prepFlowRuntime;
	}
}
